SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Indexes */

DROP INDEX erp_purchase_contract_hth ON erp_purchase_contract;



/* Drop Tables */

DROP TABLE IF EXISTS erp_purchase_contract_sub;
DROP TABLE IF EXISTS erp_purchase_contract;




/* Create Tables */

-- 业务数据表
CREATE TABLE erp_purchase_contract
(
	-- 编号
	id varchar(64) NOT NULL COMMENT '编号',
	-- 合同号
	hth varchar(100) COMMENT '合同号',
	-- 协议号
	xyh varchar(200) COMMENT '协议号',
	-- 供货单位
	ghdw varchar(200) COMMENT '供货单位',
	-- 进口国别
	jkgb varchar(50) COMMENT '进口国别',
	-- 价格条款 
	jgtk varchar(200) COMMENT '价格条款',
	-- 应预付额
	yyfe decimal(10,4) COMMENT '应预付额',
	-- 订货日期
	dhrq date COMMENT '订货日期',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记（0：正常；1：删除）',
	PRIMARY KEY (id)
) COMMENT = '业务数据表';


CREATE TABLE erp_purchase_contract_sub
(
	id varchar(64) NOT NULL,
	-- 编号
	erp_purchase_contract_id varchar(64) NOT NULL COMMENT '编号',
	-- 商品编码
	spbm varchar(100) COMMENT '商品编码',
	-- 商品名称
	spmc varchar(100) COMMENT '商品名称',
	-- 是否濒危物种
	sfbwwz varchar(50) COMMENT '是否濒危物种',
	-- 单价
	dj decimal(10,4) COMMENT '单价',
	-- 数量
	sl decimal(10,4) COMMENT '数量',
	-- 金额
	je decimal(10,4) COMMENT '金额',
	-- 创建者
	create_by varchar(64) NOT NULL COMMENT '创建者',
	-- 创建时间
	create_date datetime NOT NULL COMMENT '创建时间',
	-- 更新者
	update_by varchar(64) NOT NULL COMMENT '更新者',
	-- 更新时间
	update_date datetime NOT NULL COMMENT '更新时间',
	-- 备注信息
	remarks varchar(255) COMMENT '备注信息',
	-- 删除标记（0：正常；1：删除）
	del_flag char(1) DEFAULT '0' NOT NULL COMMENT '删除标记（0：正常；1：删除）',
	PRIMARY KEY (id)
);



/* Create Foreign Keys */

ALTER TABLE erp_purchase_contract_sub
	ADD FOREIGN KEY (erp_purchase_contract_id)
	REFERENCES erp_purchase_contract (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



/* Create Indexes */

CREATE INDEX erp_purchase_contract_hth ON erp_purchase_contract (hth ASC);



