<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购合同管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
		function addRow(list, idx, tpl, row){
			$(list).append(Mustache.render(tpl, {
				idx: idx, delBtn: true, row: row
			}));
			$(list+idx).find("select").each(function(){
				$(this).val($(this).attr("data-value"));
			});
			$(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
				var ss = $(this).attr("data-value").split(',');
				for (var i=0; i<ss.length; i++){
					if($(this).val() == ss[i]){
						$(this).attr("checked","checked");
					}
				}
			});
		}
		function delRow(obj, prefix){
			var id = $(prefix+"_id");
			var delFlag = $(prefix+"_delFlag");
			if (id.val() == ""){
				$(obj).parent().parent().remove();
			}else if(delFlag.val() == "0"){
				delFlag.val("1");
				$(obj).html("&divide;").attr("title", "撤销删除");
				$(obj).parent().parent().addClass("error");
			}else if(delFlag.val() == "1"){
				delFlag.val("0");
				$(obj).html("&times;").attr("title", "删除");
				$(obj).parent().parent().removeClass("error");
			}
		}
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/purchase/erpPurchaseContract/">采购合同列表</a></li>
		<li class="active"><a href="${ctx}/purchase/erpPurchaseContract/form?id=${erpPurchaseContract.id}">采购合同<shiro:hasPermission name="purchase:erpPurchaseContract:edit">${not empty erpPurchaseContract.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="purchase:erpPurchaseContract:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="erpPurchaseContract" action="${ctx}/purchase/erpPurchaseContract/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">合同号：</label>
			<div class="controls">
				<form:input path="hth" htmlEscape="false" maxlength="100" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">协议号：</label>
			<div class="controls">
				<form:input path="xyh" htmlEscape="false" maxlength="200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">供货单位：</label>
			<div class="controls">
				<form:input path="ghdw" htmlEscape="false" maxlength="200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">进口国别：</label>
			<div class="controls">
				<form:input path="jkgb" htmlEscape="false" maxlength="50" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">价格条款：</label>
			<div class="controls">
				<form:input path="jgtk" htmlEscape="false" maxlength="200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">应预付额：</label>
			<div class="controls">
				<form:input path="yyfe" htmlEscape="false" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">订货日期：</label>
			<div class="controls">
				<input name="dhrq" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${erpPurchaseContract.dhrq}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">备注信息：</label>
			<div class="controls">
				<form:textarea path="remarks" htmlEscape="false" rows="4" maxlength="255" class="input-xxlarge "/>
			</div>
		</div>
			<div class="control-group">
				<label class="control-label">erp_purchase_contract_sub：</label>
				<div class="controls">
					<table id="contentTable" class="table table-striped table-bordered table-condensed">
						<thead>
							<tr>
								<th class="hide"></th>
								<th>商品编码</th>
								<th>商品名称</th>
								<th>是否濒危物种</th>
								<th>单价</th>
								<th>数量</th>
								<th>金额</th>
								<th>备注信息</th>
								<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><th width="10">&nbsp;</th></shiro:hasPermission>
							</tr>
						</thead>
						<tbody id="erpPurchaseContractSubList">
						</tbody>
						<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><tfoot>
							<tr><td colspan="9"><a href="javascript:" onclick="addRow('#erpPurchaseContractSubList', erpPurchaseContractSubRowIdx, erpPurchaseContractSubTpl);erpPurchaseContractSubRowIdx = erpPurchaseContractSubRowIdx + 1;" class="btn">新增</a></td></tr>
						</tfoot></shiro:hasPermission>
					</table>
					<script type="text/template" id="erpPurchaseContractSubTpl">//<!--
						<tr id="erpPurchaseContractSubList{{idx}}">
							<td class="hide">
								<input id="erpPurchaseContractSubList{{idx}}_id" name="erpPurchaseContractSubList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="erpPurchaseContractSubList{{idx}}_delFlag" name="erpPurchaseContractSubList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_spbm" name="erpPurchaseContractSubList[{{idx}}].spbm" type="text" value="{{row.spbm}}" maxlength="100" class="input-small "/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_spmc" name="erpPurchaseContractSubList[{{idx}}].spmc" type="text" value="{{row.spmc}}" maxlength="100" class="input-small "/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_sfbwwz" name="erpPurchaseContractSubList[{{idx}}].sfbwwz" type="text" value="{{row.sfbwwz}}" maxlength="50" class="input-small "/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_dj" name="erpPurchaseContractSubList[{{idx}}].dj" type="text" value="{{row.dj}}" class="input-small "/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_sl" name="erpPurchaseContractSubList[{{idx}}].sl" type="text" value="{{row.sl}}" class="input-small "/>
							</td>
							<td>
								<input id="erpPurchaseContractSubList{{idx}}_je" name="erpPurchaseContractSubList[{{idx}}].je" type="text" value="{{row.je}}" class="input-small "/>
							</td>
							<td>
								<textarea id="erpPurchaseContractSubList{{idx}}_remarks" name="erpPurchaseContractSubList[{{idx}}].remarks" rows="4" maxlength="255" class="input-small ">{{row.remarks}}</textarea>
							</td>
							<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#erpPurchaseContractSubList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td></shiro:hasPermission>
						</tr>//-->
					</script>
					<script type="text/javascript">
						var erpPurchaseContractSubRowIdx = 0, erpPurchaseContractSubTpl = $("#erpPurchaseContractSubTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
						$(document).ready(function() {
							var data = ${fns:toJson(erpPurchaseContract.erpPurchaseContractSubList)};
							for (var i=0; i<data.length; i++){
								addRow('#erpPurchaseContractSubList', erpPurchaseContractSubRowIdx, erpPurchaseContractSubTpl, data[i]);
								erpPurchaseContractSubRowIdx = erpPurchaseContractSubRowIdx + 1;
							}
						});
					</script>
				</div>
			</div>
		<div class="form-actions">
			<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>