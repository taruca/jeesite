<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>采购合同管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/purchase/erpPurchaseContract/">采购合同列表</a></li>
		<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><li><a href="${ctx}/purchase/erpPurchaseContract/form">采购合同添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="erpPurchaseContract" action="${ctx}/purchase/erpPurchaseContract/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>合同号：</label>
				<form:input path="hth" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>供货单位：</label>
				<form:input path="ghdw" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li><label>进口国别：</label>
				<form:input path="jkgb" htmlEscape="false" maxlength="50" class="input-medium"/>
			</li>
			<li><label>订货日期：</label>
				<input name="beginDhrq" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${erpPurchaseContract.beginDhrq}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endDhrq" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${erpPurchaseContract.endDhrq}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>合同号</th>
				<th>协议号</th>
				<th>供货单位</th>
				<th>进口国别</th>
				<th>应预付额</th>
				<th>订货日期</th>
				<th>更新时间</th>
				<th>备注信息</th>
				<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="erpPurchaseContract">
			<tr>
				<td><a href="${ctx}/purchase/erpPurchaseContract/form?id=${erpPurchaseContract.id}">
					${erpPurchaseContract.hth}
				</a></td>
				<td>
					${erpPurchaseContract.xyh}
				</td>
				<td>
					${erpPurchaseContract.ghdw}
				</td>
				<td>
					${erpPurchaseContract.jkgb}
				</td>
				<td>
					${erpPurchaseContract.yyfe}
				</td>
				<td>
					<fmt:formatDate value="${erpPurchaseContract.dhrq}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					<fmt:formatDate value="${erpPurchaseContract.updateDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${erpPurchaseContract.remarks}
				</td>
				<shiro:hasPermission name="purchase:erpPurchaseContract:edit"><td>
    				<a href="${ctx}/purchase/erpPurchaseContract/form?id=${erpPurchaseContract.id}">修改</a>
					<a href="${ctx}/purchase/erpPurchaseContract/delete?id=${erpPurchaseContract.id}" onclick="return confirmx('确认要删除该采购合同吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>