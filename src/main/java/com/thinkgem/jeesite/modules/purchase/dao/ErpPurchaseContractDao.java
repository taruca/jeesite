/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.purchase.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.purchase.entity.ErpPurchaseContract;

/**
 * 采购合同DAO接口
 * @author 孙佳亮
 * @version 2016-06-03
 */
@MyBatisDao
public interface ErpPurchaseContractDao extends CrudDao<ErpPurchaseContract> {
	
}