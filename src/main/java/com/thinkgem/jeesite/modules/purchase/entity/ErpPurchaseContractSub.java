/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.purchase.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 采购合同Entity
 * @author 孙佳亮
 * @version 2016-06-03
 */
public class ErpPurchaseContractSub extends DataEntity<ErpPurchaseContractSub> {
	
	private static final long serialVersionUID = 1L;
	private ErpPurchaseContract erpPurchaseContractId;		// 编号 父类
	private String spbm;		// 商品编码
	private String spmc;		// 商品名称
	private String sfbwwz;		// 是否濒危物种
	private String dj;		// 单价
	private String sl;		// 数量
	private String je;		// 金额
	
	public ErpPurchaseContractSub() {
		super();
	}

	public ErpPurchaseContractSub(String id){
		super(id);
	}

	public ErpPurchaseContractSub(ErpPurchaseContract erpPurchaseContractId){
		this.erpPurchaseContractId = erpPurchaseContractId;
	}

	@Length(min=1, max=64, message="编号长度必须介于 1 和 64 之间")
	public ErpPurchaseContract getErpPurchaseContractId() {
		return erpPurchaseContractId;
	}

	public void setErpPurchaseContractId(ErpPurchaseContract erpPurchaseContractId) {
		this.erpPurchaseContractId = erpPurchaseContractId;
	}
	
	@Length(min=0, max=100, message="商品编码长度必须介于 0 和 100 之间")
	public String getSpbm() {
		return spbm;
	}

	public void setSpbm(String spbm) {
		this.spbm = spbm;
	}
	
	@Length(min=0, max=100, message="商品名称长度必须介于 0 和 100 之间")
	public String getSpmc() {
		return spmc;
	}

	public void setSpmc(String spmc) {
		this.spmc = spmc;
	}
	
	@Length(min=0, max=50, message="是否濒危物种长度必须介于 0 和 50 之间")
	public String getSfbwwz() {
		return sfbwwz;
	}

	public void setSfbwwz(String sfbwwz) {
		this.sfbwwz = sfbwwz;
	}
	
	public String getDj() {
		return dj;
	}

	public void setDj(String dj) {
		this.dj = dj;
	}
	
	public String getSl() {
		return sl;
	}

	public void setSl(String sl) {
		this.sl = sl;
	}
	
	public String getJe() {
		return je;
	}

	public void setJe(String je) {
		this.je = je;
	}
	
}