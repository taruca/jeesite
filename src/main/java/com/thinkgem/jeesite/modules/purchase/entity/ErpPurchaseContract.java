/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.purchase.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.google.common.collect.Lists;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 采购合同Entity
 * @author 孙佳亮
 * @version 2016-06-03
 */
public class ErpPurchaseContract extends DataEntity<ErpPurchaseContract> {
	
	private static final long serialVersionUID = 1L;
	private String hth;		// 合同号
	private String xyh;		// 协议号
	private String ghdw;		// 供货单位
	private String jkgb;		// 进口国别
	private String jgtk;		// 价格条款
	private String yyfe;		// 应预付额
	private Date dhrq;		// 订货日期
	private Date beginDhrq;		// 开始 订货日期
	private Date endDhrq;		// 结束 订货日期
	private List<ErpPurchaseContractSub> erpPurchaseContractSubList = Lists.newArrayList();		// 子表列表
	
	public ErpPurchaseContract() {
		super();
	}

	public ErpPurchaseContract(String id){
		super(id);
	}

	@Length(min=0, max=100, message="合同号长度必须介于 0 和 100 之间")
	public String getHth() {
		return hth;
	}

	public void setHth(String hth) {
		this.hth = hth;
	}
	
	@Length(min=0, max=200, message="协议号长度必须介于 0 和 200 之间")
	public String getXyh() {
		return xyh;
	}

	public void setXyh(String xyh) {
		this.xyh = xyh;
	}
	
	@Length(min=0, max=200, message="供货单位长度必须介于 0 和 200 之间")
	public String getGhdw() {
		return ghdw;
	}

	public void setGhdw(String ghdw) {
		this.ghdw = ghdw;
	}
	
	@Length(min=0, max=50, message="进口国别长度必须介于 0 和 50 之间")
	public String getJkgb() {
		return jkgb;
	}

	public void setJkgb(String jkgb) {
		this.jkgb = jkgb;
	}
	
	@Length(min=0, max=200, message="价格条款长度必须介于 0 和 200 之间")
	public String getJgtk() {
		return jgtk;
	}

	public void setJgtk(String jgtk) {
		this.jgtk = jgtk;
	}
	
	public String getYyfe() {
		return yyfe;
	}

	public void setYyfe(String yyfe) {
		this.yyfe = yyfe;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getDhrq() {
		return dhrq;
	}

	public void setDhrq(Date dhrq) {
		this.dhrq = dhrq;
	}
	
	public Date getBeginDhrq() {
		return beginDhrq;
	}

	public void setBeginDhrq(Date beginDhrq) {
		this.beginDhrq = beginDhrq;
	}
	
	public Date getEndDhrq() {
		return endDhrq;
	}

	public void setEndDhrq(Date endDhrq) {
		this.endDhrq = endDhrq;
	}
		
	public List<ErpPurchaseContractSub> getErpPurchaseContractSubList() {
		return erpPurchaseContractSubList;
	}

	public void setErpPurchaseContractSubList(List<ErpPurchaseContractSub> erpPurchaseContractSubList) {
		this.erpPurchaseContractSubList = erpPurchaseContractSubList;
	}
}