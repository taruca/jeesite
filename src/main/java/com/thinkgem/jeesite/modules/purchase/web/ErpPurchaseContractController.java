/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.purchase.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.purchase.entity.ErpPurchaseContract;
import com.thinkgem.jeesite.modules.purchase.service.ErpPurchaseContractService;

/**
 * 采购合同Controller
 * @author 孙佳亮
 * @version 2016-06-03
 */
@Controller
@RequestMapping(value = "${adminPath}/purchase/erpPurchaseContract")
public class ErpPurchaseContractController extends BaseController {

	@Autowired
	private ErpPurchaseContractService erpPurchaseContractService;
	
	@ModelAttribute
	public ErpPurchaseContract get(@RequestParam(required=false) String id) {
		ErpPurchaseContract entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = erpPurchaseContractService.get(id);
		}
		if (entity == null){
			entity = new ErpPurchaseContract();
		}
		return entity;
	}
	
	@RequiresPermissions("purchase:erpPurchaseContract:view")
	@RequestMapping(value = {"list", ""})
	public String list(ErpPurchaseContract erpPurchaseContract, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<ErpPurchaseContract> page = erpPurchaseContractService.findPage(new Page<ErpPurchaseContract>(request, response), erpPurchaseContract); 
		model.addAttribute("page", page);
		return "modules/purchase/erpPurchaseContractList";
	}

	@RequiresPermissions("purchase:erpPurchaseContract:view")
	@RequestMapping(value = "form")
	public String form(ErpPurchaseContract erpPurchaseContract, Model model) {
		model.addAttribute("erpPurchaseContract", erpPurchaseContract);
		return "modules/purchase/erpPurchaseContractForm";
	}

	@RequiresPermissions("purchase:erpPurchaseContract:edit")
	@RequestMapping(value = "save")
	public String save(ErpPurchaseContract erpPurchaseContract, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, erpPurchaseContract)){
			return form(erpPurchaseContract, model);
		}
		erpPurchaseContractService.save(erpPurchaseContract);
		addMessage(redirectAttributes, "保存采购合同成功");
		return "redirect:"+Global.getAdminPath()+"/purchase/erpPurchaseContract/?repage";
	}
	
	@RequiresPermissions("purchase:erpPurchaseContract:edit")
	@RequestMapping(value = "delete")
	public String delete(ErpPurchaseContract erpPurchaseContract, RedirectAttributes redirectAttributes) {
		erpPurchaseContractService.delete(erpPurchaseContract);
		addMessage(redirectAttributes, "删除采购合同成功");
		return "redirect:"+Global.getAdminPath()+"/purchase/erpPurchaseContract/?repage";
	}

}